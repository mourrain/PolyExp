export slr_iter!, slr_newton, vdm_jac, vdm_iter!, vdm_newton

"""
```
newtoniter(M::Matrix{T}, r::Int64, E::Vector) -> Matrix{T}
```
Apply a Newton step on M for the intersection of the linear space spanned by 
E with the tangent space of the variety of rank <= r matrices at the rank <= r 
matrix, the closest to M.
"""
function slr_iter!(M::Matrix, r::Int64, E::Vector)
    U,S,V = svd(M)
    m = size(M,1); n = size(M,2);
    N = (m-r)*(n-r)
    L = length(E)
    # For least square solving, when the matrix is square add a dummy zero row.
    if N == L
        N+=1
    end
    A = fill(0.0, N, L)
    b = fill(0.0, N)
    for i in 1:r
        S[i]=0.0
    end
    dM = U*diagm(S)*V'

    for i in 1:m-r
        for j in 1:n-r 
            for k in 1:length(E)
                A[(i-1)*(n-r)+j,k] = (transpose(U[:,r+i])*E[k]*V[:,r+j])[1]
            end
            b[(i-1)*(n-r)+j] = (transpose(U[:,r+i])*dM*V[:,r+j])[1]
        end
    end
    
    #x = pinv(A)*b
    x = A\b
    delta = norm(x)
    #println("|b| = ", norm(b), "  ||A x - b|| = ", norm(A*x-b))
    for l in 1:length(E)
        M-= E[l]*x[l] 
    end
    M, delta
end

#----------------------------------------------------------------------
function slr_newton(H0, L1, L2, N, r::Int64, eps::Float64 = 1.e-5, mxi :: Int64 = 10)
    HB = hankelbasis(L1,L2)
    E = [HB[m] for m in N]
    M = copy(H0)
    c = 0 ; delta = 1.0
    while delta > eps && c < mxi
        M, delta = slr_iter!(M,r,E)
        print("delta=", delta, "\n")
        c += 1;
    end
    M
end

#----------------------------------------------------------------------
function vdm_jac(w, Xi, L)
    n = size(Xi,2)
    r = size(Xi,1)
    J = fill(typeof(Xi[1])(0), length(L), r + r*n)

    for k in 1:r
        for i in 1:length(L)
            J[i,k] = 1.;
            for v in 1:n
                J[i,k]*= Xi[k,v]^((L[i].z)[v])
            end
            for l in 1:n 
                J[i,r+(k-1)*n+l] = w[k];
                for v in 1:n
                    if v==l
                        if (L[i].z)[v] > 0
                            J[i,r+(k-1)*n+l] *= (L[i].z)[v]*Xi[k,v]^((L[i].z)[v]-1)
                        else
                            J[i,r+(k-1)*n+l] = 0.0
                        end
                    else
                        J[i,r+(k-1)*n+l] *= Xi[k,v]^((L[i].z)[v])
                    end
                end
            end
        end
    end
    J
end

#----------------------------------------------------------------------
function vdm_iter!(w, Xi, L, s0)
    f = fill(typeof(Xi[1])(0), length(L))
    mnt = moment(w,Xi)
    for i in 1:length(L)
        f[i] = mnt(L[i].z)-s0[L[i]] 
    end
    print("|ε|=",norm(f))
    J = vdm_jac(w,Xi,L)
    dwXi = J\f
    delta = norm(dwXi)

    r = length(w)
    n = size(Xi,2)
    for i in 1:r
        w[i] -= dwXi[i]
    end
    for i in 1:r
        for j in 1:n
            Xi[i,j] -= dwXi[r+(i-1)*n+j]
        end
    end
    delta
end

#----------------------------------------------------------------------
function vdm_newton(w0, Xi0, L, s0, eps::Float64 = 1.e-5, cmax:: Int64 = 10)
    w = copy(w0)
    Xi = copy(Xi0)
    c = 0; delta = 1.0
    while delta > eps && c< cmax
        delta = vdm_iter!(w,Xi,L,s0)
        print("  δ=", delta, "\n")
        c += 1;
    end
    w, Xi
end
#----------------------------------------------------------------------
