export sos, minimize, minimize_nn, minimize_tv

#----------------------------------------------------------------------
"""
```
P = sos(p)
```
Decomposition of a polynomial as a Sum Of Squares

Example
-------
```
using PolyExpSDP
X= @ring x y
q = (1.+x^2+y)^2 + x^4+y^4
S = sos(q)

  3-element Array{Any,1}:
   -1.30656x^2 + 7.48473e-39x*y + 1.51718e-9y^2 + -0.92388y + -0.92388                   
   -3.78542e-10x^2 + -2.90563e-28x*y + 1.0y^2 + -1.30574e-17x + 1.79897e-9y + 3.78558e-10
   -0.541196x^2 + 2.32935e-35x*y + -1.03817e-9y^2 + 1.69946e-16x + 0.382683y + 0.382683  
```
"""
function sos{T}(pol::Polynomial{true,T}, eps :: Float64 = 1.e-6)
 
    d = deg(pol)
    X = variables(pol)
    L = monoms(X,div(d,2))
    N = length(L)

    mdl = Model(solver = CSDPSolver())
    
    @variable(mdl, P[1:N,1:N], SDP)

    C = DataStructures.OrderedDict{MultivariatePolynomials.Monomial{true},T}()

    I = DataStructures.OrderedDict{MultivariatePolynomials.Monomial{true},Vector{Vector{Int64}}}()

    for i in 1:N
        for j in 1:N
            m = L[i]*L[j]
            C[m] = zero(T)
            l = get(I, m, [])
            push!(l,[i,j])
            I[m] = l
        end
    end
    
    for t in pol
         C[t.x] = t.α
    end

    for (m,c) in C
         @constraint(mdl, sum(P[I[m][i][1],I[m][i][2]] for i in 1:length(I[m])) - c == 0)
    end
    
    @objective(mdl, Min, sum(P[i,i] for i in 1:N) )

    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl), " status: ", status)

    if status !=  :Infeasible
        P  = getvalue(P)
        # println((transpose(L)*P*L)[1])
        U,S,V = svd(P)
        r = numrank(S,eps)
        diagm([sqrt(S[i]) for i in 1:r])*transpose(U[:,1:r])*L
    end
    
end

#------------------------------------------------------------------------
"""
```julia
m, Xi = minimize(f, L, [e1, e2, ...], [p1, p2, ...]) 
```
Compute the infimum of the functional associated to the polynomial f on the moments of the monomials in L*L such that the Hankel matrix with row and columns indexed by L is Semi_definite Positive and satisfying the equality constraints ei == 0 and the sign constraints pi >= 0.

f, ei, p1 should be polynomials in the variables X.

If the problem is feasible and has minimizers, it outputs
  - m: the minimum 
  - Xi: the matrix of minimizer points (one point per row).

The minimizers are computed from the moments in degree 2d, with 
the function `svd_decompose`.

Example
-------
```julia
using PolyExpSDP

X  = @ring x1 x2
e1 = x1^2-2
e2 = (x2^2-3)*(x1*x2-2)

m, Xi = minimize(-x1, monoms(X, 3), [e1, e2], [x1, 2-x2])

  (-1.4142135624006709, [1.41421 1.73205; 1.41421 1.41421; 1.41421 -1.73205])
```
"""
function minimize(fct, L, Eq = [], Pos = [])

    un = L[1]*one(Float64);
    f  = fct*un

    N = length(L)
    X  = L[N].vars
    n  = length(X)
    d = deg(L[N])
    
    mdl = Model(solver=CSDPSolver())
    
    M = @variable(mdl, [1:N,1:N], SDP)

    # Moments of a probability measure
    @constraint(mdl, M[1,1]==1)
    
    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )

    # Hankel structure
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
	            @constraint(mdl, M[i,j]-M[id[1], id[2]] == 0)
	        end
	    end
        end
    end

    # Relations between moments for the polynomial constraints
    for eq in Eq
        p = eq*un
        mon = monoms(X,2*d-deg(p))
        for m in mon
            q = p*m*one(Float64)
            @constraint(mdl, sum(t.α*M[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
        end
    end
    
    for pos in Pos
        p  = pos*un
        Lp = monoms(X,d - deg(p))
        Np = length(Lp)
        P  = @variable(mdl, [1:Np,1:Np], SDP)
        for i in 1:Np
            for j  in 1:Np
                mn = Lp[i]*Lp[j]
                @constraint(mdl, P[i,j]-sum(t.α*M[I[exponent(t.x*mn)][1],I[exponent(t.x*mn)][2]] for t in p) == 0)
	    end
        end
    end

    # Minimize the linear functional associated to f
    @objective(mdl, Min, sum(t.α*M[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in f) )

    #println(mdl)
    
    status = solve(mdl)
    m = getobjectivevalue(mdl)

    #println("Objective value: ", m)

    if status !=  :Infeasible
        s = series(getvalue(M), L, L)
        w, Xi = svd_decompose(s);

        # TODO: check the decomposition.
        m, Xi
    end
end

#----------------------------------------------------------------------
function minimize_m(L, sigma, Eq=[])
 
    N = length(L)
    X = variables(sigma)
    n = length(X)
    d = deg(L[N])
    delta = deg(sigma)
    
    mdl = Model(solver = CSDPSolver())
    
    @variable(mdl, P[1:N,1:N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,j]-P[id[1], id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if cf != 0
                @constraint(mdl, P[i,j]-cf == 0)
	    elseif deg(mn) == delta
                @constraint(mdl, P[i,j] == 0)
            end
        end
    end
    
    #println(mdl)
    for pol in Eq
        mon = monoms(X,2*d-deg(pol))
        for m in mon
            q = pol*m
            @constraint(mdl,sum(t.α*P[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
        end
    end

    @objective(mdl, Min, P[1,1])

    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl))
    getvalue(P)
end

#----------------------------------------------------------------------
function minimize_tv(L, sigma, Eq = [])
 
    N = length(L)
    X = variables(sigma)
    n = length(X)
    d = deg(L[N])

    mdl = Model(solver=CSDPSolver())
    
    @variable(mdl, P[1:N,1:N], SDP)
    @variable(mdl, M[1:N,1:N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,j]-P[id[1], id[2]] == 0)
	            @constraint(mdl, M[i,j]-M[id[1], id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if cf != 0
                @constraint(mdl, P[i,j]-M[i,j]-cf == 0)
	    end
        end
    end

    for pol in Eq
        mon = monoms(X,2*d-deg(pol))
        for m in mon
            q = pol*m
            @constraint(mdl,sum(t.α*P[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
            @constraint(mdl,sum(t.α*M[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
        end
    end
    
    @objective(mdl, Min, P[1,1]+M[1,1])

    #println(mdl)
    
    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl))
    getvalue(P), getvalue(M)
end

#----------------------------------------------------------------------
function minimize_nn(L, sigma)
 
    N = length(L)
    n = length(variables(sigma))

    mdl = Model(solver = CSDPSolver())
    
    @variable(mdl, P[1:2*N,1:2*N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,N+j]-P[id[1], N+id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if  deg(mn) <= deg(sigma) #cf != 0
                @constraint(mdl, P[i,N+j]-cf == 0)
	    end
        end
    end
    
    #println(mdl)

    @objective(mdl, Min, sum(P[i,i] for i in 1:2*N) )

    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl)/2)
    R = getvalue(P)
    R[1:N,N+1:2*N]
end

