export @ring, deg, variables, monoms, monomials, monomial, inv, norm, coeff

import Base: norm, inv
import MultivariatePolynomials: deg, monomials

function buildpolvar{PV}(::Type{PV}, arg, var)
    :($(esc(arg)) = $var)
end

"""
```julia
@ring args...
```
Defines the arguments as variables and output their array.

Example
-------
```julia
X = @ring x1 x2
```
"""
macro ring(args...)
    X = MultivariatePolynomials.PolyVar{true}[MultivariatePolynomials.PolyVar{true}(string(arg)) for arg in args]
    V = [buildpolvar(PolyVar{true}, args[i], X[i]) for i in 1:length(X)]
    push!(V, :(TMP = $X) )
    reduce((x,y) -> :($x; $y), :(), V)
end

#----------------------------------------------------------------------
"""
```
variables(p::Polynomial) -> Vector{PolyVar{true}}
```
Variables of a polynomial.
"""
function variables(p::Polynomial)
   return vars(p)
end

#----------------------------------------------------------------------
"""
```
deg(p:Polynomial) -> Int64
```
Degree of a polynomial
"""
function deg{C,T}(p::Polynomial{C,T})
  maxdeg(p.x)
end

#----------------------------------------------------------------------
function deg{C,T}(t::Term{C,T})
  deg(t.x)
end
#----------------------------------------------------------------------
function deg{T}(v::PolyVar{T})
  1
end
#----------------------------------------------------------------------
function coeff{C,T}(t::Term{C,T})
  t.α
end
#----------------------------------------------------------------------
"""
```
 monomial()
```
 return the monomial 1
"""
function monomial()
    Monomial{true}()
end

#----------------------------------------------------------------------
"""
```
 inv(m :: Monomial{true})
```
 return the inverse monomial with opposite exponents.
"""
function inv(m:: Monomial{true})
    Monomial{true}(m.vars,-m.z)
end

function inv!(m:: Monomial{true})
    m.z=-m.z
end

#-----------------------------------------------------------------------
"""
```
monoms(V, d::Int64) -> Vector{Monomial}
monoms(V, rg::UnitRangeInt64) -> Vector{Monomial}
```
List of all monomials in the variables V up to degree d of from degree d1 to d2, 
ordered by increasing degree.
"""
function monoms(V, rg::UnitRange{Int64}) 
    L = monomials(V,rg.start)
    for i in (rg.start+1):rg.stop
        append!(L,monomials(V,i))
    end
    L
end

#-----------------------------------------------------------------------
"""
```
monoms(V, d::Int64) -> Vector{Monomial}
```
List of all monomials in the variables V up to degree d of from degree d1 to d2, 
ordered by increasing degree.
"""
function monoms(V, d ::Int64) 
    if (d>0)
        monoms(V,0:d)
    else
        L = monoms(V, 0:-d)
        for i in 1:length(L)
            inv!(L[i])
        end
        L
    end
end

#-----------------------------------------------------------------------
"""
```
monomials(V, rg::UnitRangeInt64) -> Vector{Monomial}
```
List of all monomials in the variables V of degree in the rang rg.
"""
function monomials(V::Vector{PolyVar{true}}, rg::UnitRange{Int64}) 
    L = monomials(V,rg.start)
    for i in (rg.start+1):rg.stop
        append!(L,monomials(V,i))
    end
    L
end

#----------------------------------------------------------------------
function norm{C,T}(p::Polynomial{C,T}, x::Float64)
    if (x == Inf)
        r = - Inf
        for t in p
            r = max(r, abs(t.α))
        end
    else
        r = Inf
        for t in p
            r = min(r, abs(t.α))
        end
    end
    r
end

function norm{C,T}(pol::Polynomial{C,T}, p::Int64=2)
    r=sum(abs(t.α)^p for t in pol)
    exp(log(r)/p)
end
#----------------------------------------------------------------------
