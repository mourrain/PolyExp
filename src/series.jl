export series, zero, terms, convert, monomials, setindex, setindex!, dual, deg,
       +, -, *, /

import Base:
    zero, one, show, showcompact, print, length, endof, getindex, setindex!, copy, promote_rule, convert, start, next, done, eltype, 
    *, /, //, -, +, ==, ^, divrem, conj, rem, real, imag, diff, norm

import MultivariatePolynomials: deg, isconstant, nvars, show, monomials

# function hash(x::Monomial{true}, h::UInt)
#     return hash(x.z, h)
# end

#----------------------------------------------------------------------
"""
```
Series{T}
```
Class representing multivariate series with coefficients of type T. 
The series are dictionaries, which associates values of type T to monomials. 
"""
type Series{T}
    terms::OrderedDict{Monomial{true},T}
end
#----------------------------------------------------------------------

series{T}(m::Monomial{true}, c::T) =  Series{T}(OrderedDict(m => c))

"""
```
dual(p:Polynomial{C,T}) -> Series{T}
```
Construct the series from the polynomial by replacing the monomial basis by its dual basis

Example
-------
```julia
@ring x1 x2
p = x1^2*x2+3*x2+2
s = dual(p)

# output 

dx1^2dx2 + 3dx2 + 2
```

"""
function dual{C,T}(p::Polynomial{C,T})
    d = OrderedDict{Monomial{true},T}()
    for t in p
        d[t.x] = t.α
    end
    return Series(d)
end

terms(p::Series) = p.terms

eltype{T}(::Series{T}) = T

zero{T}(::Type{Series{T}}) = Series{T}(OrderedDict{Vector{Int},T}())
zero{T}(p::Series{T}) = zero(Series{T})

one{T}(::Type{Series{T}}) = Series{T}(OrderedDict(zeros(Int, length(vars)) => one(T)), vars)
one{T}(p::Series{T}) = one(Series{T}, vars=vars(p))

promote_rule{T,U}(::Type{Series{T}}, ::Type{Series{U}}) = Series{promote_type(T, U)}
promote_rule{T,U}(::Type{Series{T}}, ::Type{U}) = Series{promote_type(T, U)}

function convert{T}(P::Type{Series{T}}, p::Series)
    r = zero(P)
    for (m, c) in p
        r[m] = convert(T, c)
    end
    r
end

convert{T}(::Type{Series{T}}, c::T) =
    Series{T}(DataStructure.OrderedDict(Monomial{true}() => c))

monomials(p::Series) = keys(terms(p))

getindex{T}(p::Series{T}, m::Monomial{true}) =
    get(terms(p), m, zero(T))

getindex(p::Series, m::Int...) = p[[m...]]

function setindex!{T}(p::Series{T}, v, m::Monomial{true})
    if method_exists(isapprox, (T,T)) && isapprox(v, zero(T))
        delete!(terms(p), m)
    else
        terms(p)[m] = v
    end
end

start(p::Series) = start(terms(p))
next(p::Series, state) = next(terms(p), state)
done(p::Series, state) = done(terms(p), state)

copy{T}(p::Series{T}) = Series{T}(copy(terms(p)))

#----------------------------------------------------------------------
function variables(s::Series)
    for (m, c) in s
        return vars(m)
    end
end

#----------------------------------------------------------------------
function norm{T}(s::Series{T}, x::Float64)
    if (x == Inf)
        r = - Inf
        for (m, c) in s
            r = max(r, abs(c))
        end
    else
        r = Inf
        for (m, c) in s
            r = min(r, abs(c))
        end
    end
    r
end

function norm{T}(s::Series{T}, p::Int64=2)
    r = zero(T)
    for (m, c) in s
        r += abs(c)^p
    end
    r^(1/p)
end

#----------------------------------------------------------------------
function +(p1::Series, p2::Series)
    R = promote_type(T, U)
    r = convert(Series{R}, p1)
    for (m, c) in p2
        v = r[m] + c
        if v == zero(R)
            delete!(terms(r), m)
        else
            terms(r)[m] = v
        end
    end
    r
end

function +{T}(ps::Series{T}...)
    r = zero(Series{T})
    for pindex = 1:length(ps)
        for (m, c) in ps[pindex]
            r[m] += c
        end
    end
    r
end

function +{T,U}(p::Series{T}, s::U)
    r = copy(convert(Series{promote_type(T,U)}, p))
    r[1] += s
    r
end

+(s, p::Series) = p + s

#----------------------------------------------------------------------
function -{T,U}(s1::Series{T}, s2::Series{U})
    R = promote_type(T, U)
    r = convert(Series{R}, s1)
    for (m, c) in s2
        v = r[m] - c
        if v == zero(R)
            delete!(terms(r), m)
        else
            terms(r)[m] = v
        end
    end
    r
end

function -{T,U}(s::Series{T}, u::U)
    r = copy(convert(Series{promote_type(T, U)}, s))
    r[zeros(Int, nvars(s))] -= u
    r
end

function -(s::Series)
    r = zero(p)
    for (m, c) in s
        r[m] = -c
    end
    r
end

-(s, p::Series) = -p + s


function *(s1::Series, s2::Series)
    r = zero(s1)
    for (m1, c1) in s1
        for (m2, c2) in s2
            r[m1*m2] += c1 * c2
        end
    end
    r
end

function *{T,U}(s::T, p::Series{U})
    r = zero(Series{promote_type(T,U)})
    for (m, c) in p
        r[m] = s * c
    end
    r
end

*(p::Series, s) = s * p


function ^(p::Series, power::Integer)
    @assert power >= 0
    if power == 0
        return one(p)
    elseif power == 1
        return p
    else
        f, r = divrem(power, 2)
        return p^(f+r) * p^f
    end
end


function /{T,U}(p::Series{T}, s::U)
    r = zero(Series{promote_type(T,U)})
    for (m, c) in p
        r[m] = c/s
    end
    r
end
#----------------------------------------------------------------------
"""
```
deg(s::Series) -> Int64
```
Maximal degree of the moments defined in the seris `s`.
"""
function deg(s::Series)
    d = 0
    for (m, c) in s
        s = deg(m)
        if s > d
            d = s
        end
    end
    d
end
#----------------------------------------------------------------------

function ==(p::Series, q::Series)
    for (m, c) in p
        if !isapprox(q[m], c)
            return false
        end
    end
    for (m, c) in q
        if !isapprox(p[m], c)
            return false
        end
    end
    true
end


function show(io::IO, p::Series)
    print(io, p)
end

function printmonomial{C}(io::IO, x::Monomial{C})
    if !isconstant(x)
        needsep = false
        for i in 1:nvars(x)
            if x.z[i] != 0 
                if needsep
                    print(io, '*')
                end
                print(io,'d')
                show(io, x.vars[i])
                if x.z[i] > 1 
                    print(io, '^')
                    print(io, x.z[i])
                elseif x.z[i] < 0
                    print(io, "^(")
                    print(io, x.z[i])
                    print(io, ")")
                else
                    needsep = true
                end
            end
        end
    end
end

function print{T}(io::IO, p::Series{T})
    first = true
    for (m, c) in p
        if first
            if T <: Complex
                if deg(m) == 0
                    print(io, "$c")
                else
                    print(io, "($c)*")
                end
            else
                if deg(m) == 0 || c != one(T)
                    print(io, c)
                end
            end
            printmonomial(io, m)
            first = false
        else
            if T <: Complex
                if deg(m) == 0
                    print(io, " + $c")
                else
                    print(io, " + ($c)*")
                end
            else
                if c >= zero(T)
                    if deg(m) != 0 && c == one(T)
                        print(io, " + ")
                    else
                        print(io, " + $c")
                    end
                else
                    print(io, " - $(-c)")
                end
            end
            printmonomial(io, m)
        end
    end
    if first
        print(io, zero(T))
    end
end
