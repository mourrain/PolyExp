export contain, *, mult, expand, delta, next_monom, bbs, is_aprox_zero, is_zero;
import Base: *, expand
#------------------------------------------------------------------------
function contain(L,m)
  return m in L
end

#----------------------------------------------------------------------
function delta(B)
  X = variables(B[1]);
  Delta =  eltype(B)[];
  for i in 1:length(B)
     for v in 1:length(X)
     	 m = B[i]*X[v];
     	 if !contain(B,m) && !contain(Delta,m)
            push!(Delta,m)
	 end
     end
  end
  return Delta
end

#----------------------------------------------------------------------
function next_monom(B,D,L,X)
  #print("next", B)
  #X = variables(B[1]);
  Delta =  eltype(B)[];
  for i in 1:length(B)
     for v in 1:length(X)
     	 m = B[i]*X[v];
     	 if contain(L,m) && !contain(B,m) && !contain(D,m) && !contain(Delta,m)
            push!(Delta,m)
	 end
     end
  end
  return Delta
end

#----------------------------------------------------------------------
function new_monomial(m, X, d, IN)
    if length(IN) == 0
        return true
    end

    if (deg(m)> d)
        return false
    end

    for v in 1:length(X)
     	m1 = m*X[v]^(-1)
        if get(IN,m1,false)
            return true
	end
    end
    return false
end

#----------------------------------------------------------------------
function *(B::Vector{Monomial{true}}, x::PolyVar{true})
  Delta =  eltype(B)[];
  for i in 1:length(B)
      push!(Delta,B[i]*x)
  end
  return Delta
end

#----------------------------------------------------------------------
function *(L1::Vector{Monomial{true}}, L2::Vector{Monomial{true}})
  L =  eltype(L1)[];
  for m1 in L1
     for m2 in L2
     	 m = m1*m2
     	 if !contain(L,m)
             push!(L,m)
	 end
     end
  end
  return L
end
           
#----------------------------------------------------------------------
function redbbs(f, K, L)
   r=f
   for i in 1:length(K)
       c = get(terms(f),terms(L[i])[1],0);
       if c!= 0
       	  f -= K[i]*c
       end
   end
   r
end

#----------------------------------------------------------------------
is_aprox_zero(eps) = function(d)
  abs(d) < eps
end

function is_zero{T}(d::T)
  d == zero(T)
end
    
function is_zero(d::Float64)
    is_aprox_zero(0.0001)(d)
end        

#----------------------------------------------------------------------
"""
```
B,K,D,P,C = bbs(sigma::Series{R}, L, eqzero = is_zero)
```
Compute the border basis of the series ``σ`` from the moments indexed 
by the list of monomials L.
It outputs:
  * B the monomial basis
  * K the border relations
  * D the border monomials
  * P the orthogonal basis
  * C the dual basis

"""
function bbs{RR}(sigma::PolyExp.Series{RR}, A, eqzero = is_zero)
    X = variables(sigma)
    dA = deg(A[length(A)])    

    B = Polynomial{true,RR}[]
    C = Polynomial{true,RR}[]
    D = Polynomial{true,RR}[]

    K = Polynomial{true,RR}[]
    P = Polynomial{true,RR}[]
    
    S = copy(A)
    T = copy(A)
    In = DataStructures.OrderedDict{Monomial{true},Bool}()
    dC = 0
    dB = 0

    for m in S
    # while length(S)>0 && new_monomial(S[1], X, dA-dC, In) 
    #     m = shift!(S) 
        if !new_monomial(m, X, dA-dC, In) 
            if deg(m) <= dB 
                #print(".")
                continue
            else
                break
            end
        end
        
        p  = orthoproj(sigma,m,P,C)
        dg = dA-deg(p)

        t = T[1]; j = 1
        d = dualdot(sigma, p, t)
        l = length(T)
        i = 1

        while i <= l && eqzero(d) && deg(t) <= dg
            t = T[i]; j=i
	    d = dualdot(sigma, p, t)
  	    i += 1
        end

        if (i>l || (deg(t)>dg)) 
      	    push!(K,p)
	    push!(D,m)
            #            In[m] = true
            # println("K: m=",m, " p=", p)
        elseif(deg(t) <= dg)
	    push!(B,m)
	    push!(P,p)
 	    push!(C,t*(one(RR)/d))
            dB = max(dB, deg(m))
            dC = max(dC, deg(t))
	    deleteat!(T,j)
            In[m] = true
            # println("b=", B, ", c=", C, " p=", p)
        end
    end
    # println()
    B,K,D,P,C
end
#------------------------------------------------------------------------
function bbs{RR}(sigma::Series{RR}, A, r::Int64, eqzero = is_zero)
    X = variables(sigma)
    dmax = deg(A[length(A)])    

    B = Polynomial{true,RR}[]
    C = Polynomial{true,RR}[]
    K = Polynomial{true,RR}[]
    D = Polynomial{true,RR}[]
    P = Polynomial{true,RR}[]
    
    S = copy(A);
    N = copy(A)

    X = variables(sigma)
    dA = deg(A[length(A)])    

    B = Polynomial{true,RR}[]
    C = Polynomial{true,RR}[]
    D = Polynomial{true,RR}[]

    K = Polynomial{true,RR}[]
    P = Polynomial{true,RR}[]
    
    S = copy(A)
    T = copy(A)
    In = DataStructures.OrderedDict{Monomial{true},Bool}()
    dC = 0
    while length(S)>0 && length(B) < r && new_monomial(S[1], X, dA-dC, In) 

        #m = S[1]; deleteat!(S,1)
        m = shift!(S)
        p  = orthoproj(sigma,m,P,C)
        dg = dA-deg(p)

        t = T[1]; j = 1
        d = dualdot(sigma, p*t)
        l = length(T)
        i = 1

        while eqzero(d) && deg(t) <= dg && i <= l 
            t = T[i]; j=i
	    d = dualdot(sigma, p*t)
  	    i += 1
        end

        if (i>l || (deg(t)>dg)) 
      	    push!(K,p)
	    push!(D,m)
        elseif(deg(t) <= dg)
	    push!(B,m)
            In[m] = true
	    push!(P,p)
 	    push!(C,t*(one(RR)/d))
            dC = max(dC, deg(t))
	    deleteat!(T,j)
        end
      
    end
    for m in S
        if !new_monomial(m, X, dA-dC, In)
            break
        end
        p  = orthoproj(sigma,m,P,C)
      	push!(K,p)
	push!(D,m)
    end

    B,K,D,P,C
end
#------------------------------------------------------------------------
