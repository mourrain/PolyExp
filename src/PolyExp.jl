module PolyExp

  using DataStructures
  using MultivariatePolynomials

  include("polynomial.jl")
  include("series.jl")
  include("hankel.jl")
  include("decompose.jl")
  include("bbs.jl")
  include("sparse.jl")
  include("tensor.jl")
  include("newton.jl")
	 
end #module PolyExp

using MultivariatePolynomials

