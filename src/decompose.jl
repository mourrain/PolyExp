export orthoproj, basis, ortho_decompose, numrank, svd_decompose,ls_decompose;

#------------------------------------------------------------------------
function orthoproj(sigma,f,P,Q)
   r=f
   for i in 1:length(P)
       r -= dualdot(sigma,r,Q[i])*P[i]
   end
   r
end

#------------------------------------------------------------------------
function basis{T}(sigma::Series{T}, L1, M2)
    P = Polynomial{true,T}[]
    Q = Polynomial{true,T}[]
    K = Polynomial{true,T}[]
    eps = 0.0001;
    L2 = copy(M2);
    for j in 1:length(L1)
        pm = orthoproj(sigma, L1[j], P, Q)
	i = 1
	d = 0
        # while (i<=length(L2)) && abs(d) < eps
        while (i<=length(L2)) && d == 0
	   d = dualdot(sigma,pm,L2[i])
	   i += 1
	end

        if abs(d) > eps
	    qm=orthoproj(sigma, L2[i-1], Q, P)*(1/d)
	    push!(P,pm)
	    push!(Q,qm)
	    deleteat!(L2,i-1)
	else
	    push!(K,pm)
	end
    end
    P,Q,K
end

#----------------------------------------------------------------------
function basis(sigma,L)
    P = eltype(L)[]
    Q = eltype(L)[]
    B = eltype(L)[]
    D = eltype(L)[]
    K = eltype(L)[]
    dMax = deg(sigma)
    #print("degree max:", dMax,"\n")
    N = [L[1]]
    eps = 0.0001;
    T = copy(L);
    while length(N)>0 
      for j in 1:length(N)
        pm = orthoproj(sigma, N[j],P,Q)
	d = 0
	i = 1
	while (i <= length(T)) && (abs(d) < eps) && (deg(T[i]) + deg(pm) <= dMax)
	   d = dualdot(sigma,pm,T[i])
	   i += 1
	end

	if abs(d) > eps && (deg(T[i-1]) + deg(pm) <= dMax)
	    push!(B,N[j])
	    push!(P,pm)
	    qm = orthoproj(sigma, T[i-1],Q,P)*(1/d);
	    push!(Q,qm)
	    deleteat!(T,i-1)
	else
	    push!(D,N[j])
	    push!(K,pm)
	end	
      end
      N = next(B,D,L)

    end
    P,Q,K,B,D
end

#----------------------------------------------------------------------
"""
```
  ortho_decompose(sigma::Series{T},L) -> Vector{T}, Matrix{T}
```
Decompose the series ``\\sigma`` as a weigthed sum of exponentials using the moments
of the monomials in the list L.

Return w, ``\\Xi`` where 
 - w is the vector of weights,
 - ``\\Xi`` is the matrix of frequency points, stored per row.

Example
-------
```julia
julia> s
4.0 + 5.0dx1 + 7.0dx2 + 5.0dx1^2 + 11.0dx1*dx2 + 13.0dx2^2 - 1.0dx1^3 + 17.0dx1^2*dx2 + 23.0dx1*dx2^2 + 25.0dx2^3 - 31.0dx1^4 + 23.0dx1^3*dx2 + 41.0dx1^2*dx2^2 + 47.0dx1*dx2^3 + 49.0dx2^4

julia> ortho_decompose(s,L4)
(Complex{Float64}
[-1.000000000000012 - 0.0im,1.9999999999999993 + 0.0im,3.000000000000007 - 0.0im],

2x3 Array{Complex{Float64},2}:x
 3.0+0.0im  1.0+0.0im  2.0+0.0im
 1.0+0.0im  1.0+0.0im  2.0+0.0im)
```
"""
function ortho_decompose{T}(sigma::Series{T},L)
    P,Q = basis(sigma,L)
    X = variables(sigma);
    M = [ mult(sigma,X[i],P,Q) for i in 1:length(X) ]
    Xi = fill(zero(Complex{T}),length(X),length(P))
    W  = fill(zero(Complex{T}),length(P))
    E  = eigvecs(M[1])
    for i in 1:length(P)
	V = E[:,i]
	for j in 1:length(X)
	  Xi[j,i] = (M[j]*V)[1]/V[1]
	end	   
	U = expand(V,P)
	val = evalat(U,Xi[:,i])
	W[i] = dualdot(sigma,U)/val
    end
    W,Xi
end


#------------------------------------------------------------------------
function ratio(V1,V0)
   i = 1;
   while V0[i] == 0 
      i+=1;
   end;
   V1[i]/V0[i];
end

#------------------------------------------------------------------------
function numrank(S, eps)
  i :: Int = 1;
  while i<= length(S) && S[i]/S[1] > eps
    i+= 1;
  end
  i-1;
end

#------------------------------------------------------------------------
function svd_decompose{T}(sigma :: Series{T}, U, S, V, B0, B1, X, r::Int64)

    n = length(X)
    Un0 = transpose(U[1,1:r])
    Un1 = V[1,1:r]

    Sinv = diagm([one(T)/S[i] for i in 1:r])
    
    M = []
    for i in 1:n
        H = hankel(sigma, B0, B1*X[i])
    	push!(M, Sinv*(ctranspose(U[:,1:r])*H*V[:,1:r]))
    end
    
    M0 = M[1]
    for i in 2:n
       M0 += M[i]*rand(Float64)
    end

    E = eigvecs(M0)

    #println("Eigenval=", eigvals(M0))

    Xi = fill(zero(E[1,1]),r,n)

    w = E \ Un1

    for i in 1:r
    	for j in 1:n
	    Xi[i,j] = ratio(M[j]*E[:,i], E[:,i])
	end
    end

    D = Un0*diagm([S[i] for i in 1:r])

    for i in 1:r
    	w[i] *=  (D*E[:,i])[1]
    end

    return w, Xi
end
#------------------------------------------------------------------------
"""
```
svd_decompose(sigma :: Series{T}, eps :: Float64 = 1.e-6)
```
Decompose the series ``σ`` as a weighted sum of exponentials.
Return ``ω``, ``Ξ`` where 
 - ``ω`` is the vector of weights,
 - ``Ξ`` is the matrix of frequency points, stored per row.
The list of monomials of degree ``\\leq {d-1 \\over 2}`` are used to construct 
the Hankel matrix, where ``d`` is the maximal degree of the moments in ``σ``.

The optional argument ``eps`` is the precision used to compute the numerical rank.
Its default value is ``1.e-6``.
"""
function svd_decompose{T}(sigma :: Series{T}, eps :: Float64 = 1.e-6,
                          B0 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                          B1 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                          X  = variables(sigma))
    H = hankel(sigma, B0, B1)
    U, S, V = svd(H)       #H= U diag(S) V'
    r = numrank(S, eps)
    svd_decompose(sigma, U,S,V, B0, B1, X, r)
end

#------------------------------------------------------------------------
"""
```
svd_decompose(sigma :: Series{T}, r::Int64)
```
Decompose the series ``σ`` assuming its rank is r.
"""
function svd_decompose{T}(sigma :: Series{T}, r :: Int64,
                          B0 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                          B1 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                          X  = variables(sigma))
    H = hankel(sigma, B0, B1)
    U, S, V = svd(H)       #H= U diag(S) V'
    svd_decompose(sigma, U,S,V, B0, B1, X, r)
end

#------------------------------------------------------------------------
"""
```
svd_decompose(p :: Polynomial{true,T},  x, eps :: Float64 =1.e-6)
```
Decompose the polynomial ``pol`` as ``∑ ωi (ξi1 x1 + ... + ξin xn)ᵈ `` where 
``d`` is the degree of ``p``.

The variable ``x`` is the variable, which is substituted by 1 for the affine decomposition.

The optional argument ``eps`` is the precision used to compute the numerical rank.
Its default value is ``1.e-6``.
"""
function svd_decompose{T}(pol :: Polynomial{true,T},  x, eps :: Float64 = 1.e-6)
    d  = deg(pol)
    sigma = series(pol, x, d)
    X = variables(pol)
    v = 0
    for i in 1:length(X)
        if X[i] == x
            v=i; break
        end
    end
    w, Xi = svd_decompose(sigma, eps)
    homog(w,Xi,d,v)
end

#------------------------------------------------------------------------
"""
```
svd_decompose(p :: Polynomial{true,T}, x, r::Int64)
```
Decompose the polynomial ``pol`` as ``∑ ωi (ξi1 x1 + ... + ξin xn)ᵈ `` where 
``d`` is the degree of ``p`` assuming the rank is ``r``.

The variable ``x`` is the variable, which is substituted by 1 for the affine decomposition.
"""
function svd_decompose{T}(pol :: Polynomial{true,T}, x, r :: Int64)
      d  = deg(pol)
    sigma = series(pol, x, d)
    X = variables(pol)
    v = 0
    for i in 1:length(X)
        if X[i] == x
            v=i; break
        end
    end
    w, Xi = svd_decompose(sigma, r)
    homog(w,Xi,d,v)
end

#------------------------------------------------------------------------
function sparse_decompose(f, zeta, X, d:: Int64)
    sigma = series(moment(f,zeta), monoms(X,d))
    w, Xi = svd_decompose(sigma)
    w, log(Xi,zeta)
end

#------------------------------------------------------------------------
function ls_decompose{T}(sigma :: PolyExp.Series{T}, U, S, V, L0, L1, X, r::Int64)

    n   = length(X)
    N   = length(L1)

    H0  = U[1:N,1:r]
    
    M = []
    I = DataStructures.OrderedDict( L0[1] => 1)
    for i in 2:length(L0)
     I[L0[i]]=i   
    end
        
    for i in 1:n
        H = fill(zero(T), N, r)
        L = L1*X[i]
        for k in 1:N
            m = get(I,L[k],0)
            if m!=0
                for l in 1:r
                    H[k,l] = U[m,l]
                end
            end
        end
        Mi = H0\H
#        println("M",i," = ", Mi)
        push!(M, Mi)
    end            
    
    M0 = sum(M[i]*rand(Float64) for i in 1:n)
    E  = eigvecs(M0)

#    println("Eigenval=", E)

    Xi = fill(zero(E[1,1]),r,n)

    for i in 1:r
    	for j in 1:n
	    Xi[i,j] = PolyExp.ratio(M[j]*E[:,i], E[:,i])
	end
    end

    Un1 = diagm(S[1:r])*V[1,1:r]
    w = E \ Un1

#    println(w)

    Un0 = transpose(U[1,1:r])

    for i in 1:r
    	w[i] *=  (Un0*E[:,i])[1]
    end

    return w, Xi
end

#----------------------------------------------------------------------
function ls_decompose{T}(sigma :: Series{T}, eps :: Float64 = 1.e-6,
                         L0 = monoms(variables(sigma), div(deg(sigma)+1,2) ),
                         L1 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                         X = variables(sigma))
    H = hankel(sigma, L0, L1)
    U, S, V = svd(H)       #H= U diag(S) V'
    r = numrank(S, eps)
    ls_decompose(sigma, U, S, V, L0, L1, X, r)
end

#----------------------------------------------------------------------
function ls_decompose{T}(sigma :: Series{T}, r ::Int64,
                         L0 = monoms(variables(sigma), div(deg(sigma)+1,2) ),
                         L1 = monoms(variables(sigma), div(deg(sigma)-1,2) ),
                         X = variables(sigma))
    H = hankel(sigma, L0, L1)
    U, S, V = svd(H)       #H= U diag(S) V'
    ls_decompose(sigma, U, S, V, L0, L1, X, r)
end
#------------------------------------------------------------------------
