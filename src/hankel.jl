export variables, 
       exponent, 
       dualdot,
       series,
       mult,
       hankel, hankelbasis,
       perturb, scale, scale!

import Base: exponent, scale, scale!, expand


"""
``` 
exponent(m::Monomial) -> Array{Int64,1} 
```
Get the exponent of a monomial as a array of Int64
"""
function exponent(m::Monomial)
    return m.z
end

function dualdot{C,T}(sigma::Series{T}, m::Monomial{C})
    return sigma[m]
end

function dualdot{C,T,U}(sigma::Series{T}, t::Term{C,U})
    return t.α*sigma[t.x]
end

function dualdot{C,T,U}(sigma::Series{T}, p::Polynomial{C,U})
    r = zero(eltype(p))
    for t in p
        r+= t.α*sigma[t.x]
    end
    return r
end

function dualdot{T}(sigma::Series{T}, p, q)
    m = p*q
    dualdot(sigma, m)
end

@doc """
```
dualdot(sigma::Series{T}, p::Monomial) -> T
dualdot(sigma::Series{T}, p::Term) -> T
dualdot(sigma::Series{T}, p::Polynomial) -> T
dualdot(sigma::Series{T}, p::Polynomial, q::Polynomial) -> T
```
Compute the dot product ``‹ p, q ›_{σ} = ‹ σ | p q ›`` or  ``‹ σ | p ›`` for p, q polynomials, terms or monomials.
""" dualdot

"""
Evaluate a polynomial p at a point x;

## Example
```
julia> p = x1^2+x1*x2;

julia> p([1.0,0.5])
1.5
```
"""
function (p::Polynomial)(x)
   r = zero(eltype(p));
   for m in p
      t=m.α
      for i in 1:length(m.x.z)
      	 t*=x[i]^m.x.z[i]
      end
      r+=t
   end
   r
end

#------------------------------------------------------------------------
""" 
```
series(f,L) -> Series{T}
```
Compute the generating series ``\\sum_{x^{α} \\in L} f(α) z^α``
for a function  ``f: \\mathbb{N}^n \\rightarrow T`` and a sequence L of monomials.
"""
function series(f,L)
   res = series(L[1],f(L[1].z))
   for m in L
         res[m] = f(m.z)
   end
   res
end;

#------------------------------------------------------------------------
""" 
```
series(H,L1, L2) -> Series{T}
```
Compute the series associated to the Hankel matrix H, with rows (resp. columns) 
indexed by the array of monomials L1 (resp. L2).
"""
function series{T, M}(H::Array{T,2} , L1::Vector{M}, L2::Vector{M})
    res = zero(Series{T})
    i= 1
    for m1  in L1
        j=1
        for m2 in L2
            m = m1*m2
	    cf = res[m]
            if cf == zero(T)
	        res[m] = H[i,j]
	    end
            j+=1
        end
        i+=1
    end
   res
end


#-----------------------------------------------------------------------
"""
```
hankel(sigma::Series{T}, L1, L2) -> Array{T,2}
```
Hankel matrix of ``σ`` with the rows indexed by the list of 
polynomials L1 and the columns by L2. The entries are the dot product for ``σ`` 
of the corresponding elements in L1 and L2.

Example
-------
```jldoctest
julia> L =[1, x1, x2, x1^2, x1*x2, x2^2]

julia> H = hankel(s,L,L)
6x6 Array{Float64,2}:
  4.0   5.0   7.0    5.0  11.0  13.0
  5.0   5.0  11.0   -1.0  17.0  23.0
  7.0  11.0  13.0   17.0  23.0  25.0
  5.0  -1.0  17.0  -31.0  23.0  41.0
 11.0  17.0  23.0   23.0  41.0  47.0
 13.0  23.0  25.0   41.0  47.0  49.0
```
"""
function hankel{T}(sigma::Series{T}, L1, L2)
   H = fill(zero(T), (length(L1), length(L2)));
   for i in 1:length(L1)
      for j in 1:length(L2)
         H[i,j] = dualdot(sigma, L1[i], L2[j])
      end
   end
   H
end

#------------------------------------------------------------------------
function red(f,P,Q)
   r=f
   for i in 1:length(P)
       r-=dualdot(sigma,r,Q[i])*P[i]
   end
   r
end

#----------------------------------------------------------------------
function mult(sigma, x, P,Q)
    M = fill(zero(Float64),(length(P),length(Q)));
    for i in 1:length(P)
        for j in 1:length(Q)
	    M[j,i] = dualdot(sigma,x*P[i],Q[j])
	end
    end
    M
end

#----------------------------------------------------------------------
function perturb(sigma, eps)
    r = zero(sigma)
    for (m, c) in sigma
        r[m] = c+ (eps*rand()*2.0-eps)
    end
    return r
end

#----------------------------------------------------------------------
""" 
Scale the moments ``σ_α`` by ``λ^{deg(α)}``.
"""
function scale(sigma::Series, lambda)
    r = zero(sigma)
    for (m, c) in sigma
        r[m] = c*lambda^deg(m)
    end
    return r
end

#----------------------------------------------------------------------
""" 
Scale the moments ``σ_α`` by ``λ^{deg(α)}``, overwriting ``σ``
"""
function scale!(s::Series, lambda)
    for (m, c) in s
        s[m] *= lambda^deg(m)
    end
    return s
end

#----------------------------------------------------------------------
function expand(V,P)
    r = zero(P[1]);
    for i in 1:length(P)
       r+= P[i]*V[i]
    end
    r
end

#----------------------------------------------------------------------
function hankelbasis(L1,L2)
    m1 = length(L1)
    m2 = length(L2)
    table = DataStructures.OrderedDict( L1[1]*L2[1] => fill(0.,m1,m2))
    for i in 1:m1
        for j in 1:m2
            M = get(table,L1[i]*L2[j], 0)
            if M != 0
                M[i,j]=1.0
            else
                M = fill(0.,m1, m2)
                M[i,j]=1.0
                table[L1[i]*L2[j]] = M
            end
        end
    end
    table
end
