export sdp1_hankel, sdp2_hankel,
   nn_hankel, norm_ncl, sos

function norm_ncl(M)
    U,S,V = svd(M)
    sum(S[i] for i in 1:length(S))
end

function sdp1_hankel(sigma, L, Lpol=[])
 
    N = length(L)
    X = variables(sigma)
    n = length(X)
    d = deg(L[N])
    delta = deg(sigma)
    
    mdl = Model(solver = CSDPSolver())
    
    @variable(mdl, P[1:N,1:N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,j]-P[id[1], id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if cf != 0
                @constraint(mdl, P[i,j]-cf == 0)
	    elseif deg(mn) == delta
                @constraint(mdl, P[i,j] == 0)
            end
        end
    end
    
    #println(mdl)
    for pol in Lpol
        mon = monoms(X,2*d-deg(pol))
        for m in mon
            q = pol*m
            @constraint(mdl,sum(t.α*P[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
        end
    end

    @objective(mdl, Min, P[1,1])

    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl))
    getvalue(P)
end

function sdp2_hankel(sigma, L, Lpol = [])
 
    N = length(L)
    X = variables(sigma)
    n = length(X)
    d = deg(L[N])

    mdl = Model(solver=CSDPSolver())
    
    @variable(mdl, P[1:N,1:N], SDP)
    @variable(mdl, M[1:N,1:N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,j]-P[id[1], id[2]] == 0)
	            @constraint(mdl, M[i,j]-M[id[1], id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if cf != 0
                @constraint(mdl, P[i,j]-M[i,j]-cf == 0)
	    end
        end
    end

    for pol in Lpol
        mon = monoms(X,2*d-deg(pol))
        for m in mon
            q = pol*m
            @constraint(mdl,sum(t.α*P[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
            @constraint(mdl,sum(t.α*M[I[exponent(t.x)][1],I[exponent(t.x)][2]] for t in q)==0)
        end
    end
    @objective(mdl, Min, P[1,1]+M[1,1])

    #println(mdl)
    
    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl))
    getvalue(P), getvalue(M)
end

function nn_hankel(sigma, L)
 
    N = length(L)
    n = length(variables(sigma))

    mdl = Model(solver = CSDPSolver())
    
    @variable(mdl, P[1:2*N,1:2*N], SDP)

    I = DataStructures.OrderedDict( fill(-1,n) => [0,0] )
    
    for i in 1:N
        for j  in 1:N
            mn = L[i]*L[j]
    	    alpha = exponent(mn)
	    id = get(I,alpha, [0,0])
	    if id == [0,0]
	        I[alpha] = [i,j];
	    else
	        if (i != id[2]) || (j != id[1])
                    @constraint(mdl, P[i,N+j]-P[id[1], N+id[2]] == 0)
	        end
	    end
	    cf = sigma[mn]
	    if cf != 0
                @constraint(mdl, P[i,N+j]-cf == 0)
	    end
        end
    end
    
    #println(mdl)

    @objective(mdl, Min, sum(P[i,i] for i in 1:2*N) )

    status = solve(mdl)
    println("Objective value: ", getobjectivevalue(mdl)/2)
    R = getvalue(P)
    R[1:N,N+1:2*N]
end

