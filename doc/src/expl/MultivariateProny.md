
# Multivariate exponential decompositon


```julia
using PolyExp
```

We consider the following function, which is a sum of 6 complex exponentials 


```julia
f = (u,v) -> 0.5*cos(0.7*pi*(u+v))+0.6*sin(4*pi*u)-0.2*cos(pi*v);
```

![waves](waves.png)

In order to recover the frequencies or exponents of these exponential terms and their coefficients, we sample the function on a grid ``({\alpha_1 \over T}, {\alpha_2 \over T}), \alpha=(\alpha_1,\alpha_2)\in A\subset \mathbb{N}^2``. This defines a sequence of moments ``\sigma_{\alpha}=f({\alpha_1 \over T}, {\alpha_2 \over T})``. We compute its generating series truncated in degree ``\leq 5``.


```julia
x = @ring x1 x2
L = monoms(x,5)
T = 10
mnt = (V->f(V[1]/T,V[2]/T))
sigma = series(mnt, L)
```




    0.3 + 0.8585922907464658dx1 + 0.29774707771034303dx2 + 0.6050846776084937dx1^2 + 0.8328361327510712dx1*dx2 + 0.2906101273580203dx2^2 - 0.15759364518763863dx1^3 + 0.5575373543042985dx1^2dx2 + 0.8039080170899477dx1*dx2^2 + 0.2775204557293506dx2^3 - 0.4519219149027473dx1^4 - 0.22417045976016967dx1^3dx2 + 0.5095797473748392dx1^2dx2^2 + 0.7717888541929423dx1*dx2^3 + 0.2569085959993554dx2^4 + 0.02699524986977328dx1^5 - 0.5338499631663495dx1^4dx2 - 0.2874793003806999dx1^3dx2^2 + 0.46210935078676274dx1^2dx2^3 + 0.7358257607718759dx1*dx2^4 + 0.22699524986977343dx2^5



Computing its decomposition using svd


```julia
w, Xi = svd_decompose(sigma);
```

yields the weights `w` of the exponential terms in f and the exponentials `Xi`:


```julia
w
```




    6-element Array{Complex{Float64},1}:
       3.59435e-14-0.3im
       3.55826e-14+0.3im
     0.25+1.33717e-12im 
     0.25-1.33724e-12im 
     -0.1-1.53513e-12im 
     -0.1+1.53519e-12im 



By taking the log and scaling by ``{T\over \pi}``, we recover the frequency vectors within precision `1O^{-11}`. 


```julia
log(Xi)*T/pi
```




    6×2 Array{Complex{Float64},2}:
               1.51673e-13+4.0im  -5.19491e-14-7.6854e-15im 
               1.51673e-13-4.0im  -5.19491e-14+7.6854e-15im 
               3.13739e-12+0.7im          -5.37933e-13+0.7im
               3.13739e-12-0.7im          -5.37933e-13-0.7im
     -3.22296e-12-8.98952e-12im            3.20726e-12+1.0im
     -3.22296e-12+8.98952e-12im            3.20726e-12-1.0im


