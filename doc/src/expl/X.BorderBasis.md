
# Border basis


```julia
using PolyExp
```


```julia
x = @ring x1 x2        # 2 variables
f = function(e)        # a moment function (Vector{Int64} -> Float64)
    2^e[1]*3^e[2]-3*4^e[1]+1.0
end
L = monoms(x,5)        # list of monomials of degree <= 5
sigma = series(f, L)   # generating series (in the dual monomials)
```




    -1.0 - 9.0dx1 + dx2 - 43.0dx1^2 - 5.0dx1*dx2 + 7.0dx2^2 - 183.0dx1^3 - 35.0dx1^2dx2 + 7.0dx1*dx2^2 + 25.0dx2^3 - 751.0dx1^4 - 167.0dx1^3dx2 - 11.0dx1^2dx2^2 + 43.0dx1*dx2^3 + 79.0dx2^4 - 3039.0dx1^5 - 719.0dx1^4dx2 - 119.0dx1^3dx2^2 + 61.0dx1^2dx2^3 + 151.0dx1*dx2^4 + 241.0dx2^5




```julia
B, K, P = bbs(sigma,L); # Border basis of sigma using the moments of L
```


```julia
B                       # Basis of the quotient
```




    3-element Array{MultivariatePolynomials.Polynomial{true,Float64},1}:
     1.0
     x1 
     x2 




```julia
K                       # Border relations
```




    3-element Array{MultivariatePolynomials.Polynomial{true,Float64},1}:
     x1^2 + -5.0x1 + 1.0x2 + 3.0         
     x1*x2 + -1.0x1 + -2.0x2 + 2.0       
     x2^2 + -6.66134e-16x1 + -4.0x2 + 3.0




```julia
P                       # Orthogonal basis of the quotient for sigma
```




    3-element Array{MultivariatePolynomials.Polynomial{true,Float64},1}:
     1.0                       
     x1 + -9.0                 
     0.368421x1 + x2 + -2.31579




```julia

```
