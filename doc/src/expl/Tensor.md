
# Symmetric tensors


```julia
using PolyExp
```

Using series and polynomials in 2 variables


```julia
x = @ring x1 x2;
```

A symmetric tensor of order d=4 and of rank 3.


```julia
d=4; t = (1.0+x1+0.75x2)^4 + 1.5*(1.0-x1)^4 -2.0*(1.0-x2)^4
```




    2.5x1^4 + 3.0x1^3x2 + 3.375x1^2x2^2 + 1.6875x1*x2^3 + -1.68359375x2^4 + -2.0x1^3 + 9.0x1^2x2 + 6.75x1*x2^2 + 9.6875x2^3 + 15.0x1^2 + 9.0x1*x2 + -8.625x2^2 + -2.0x1 + 11.0x2 + 0.5



By homogeneisation, we obtain the homogeneous polynomial ``(x_0+x_1+0.75x_2)^4 + 1.5(x_0-x_1)^4 -2(x_0-x_2)^4``, which graph in polar coordinates on the sphere looks like this:

![tensor](tensor.png)

The associated (truncated) series in the dual variables:


```julia
s = series(t,d)
```




    2.5dx1^4 + 0.75dx1^3dx2 + 0.5625dx1^2dx2^2 + 0.421875dx1*dx2^3 - 1.68359375dx2^4 - 0.5dx1^3 + 0.75dx1^2dx2 + 0.5625dx1*dx2^2 + 2.421875dx2^3 + 2.5dx1^2 + 0.75dx1*dx2 - 1.4375dx2^2 - 0.5dx1 + 2.75dx2 + 0.5



Computing its decomposition


```julia
w, Xi = svd_decompose(s);
```

yields the weights `w`


```julia
w
```




    3-element Array{Float64,1}:
      1.0
     -2.0
      1.5



and the corresponding points `Xi` which are the coefficients of ``x_1, x_2`` in the linear forms of the decomposition of the tensor t:


```julia
Xi
```




    3×2 Array{Complex{Float64},2}:
             1.0+0.0im          0.75+0.0im
     2.26043e-16+0.0im          -1.0+0.0im
            -1.0+0.0im  -1.51187e-15+0.0im


