
# Weighted sum of Dirac Measures 


```julia
using PolyExp
```

Series with 3 variables


```julia
x = @ring x1 x2 x3
n = length(x)
r = 4;
```

Random weights in [0,1]


```julia
w0 = rand(Float64,r)
```




    4-element Array{Float64,1}:
     0.25623 
     0.813766
     0.179634
     0.491103



Random points in [0,1]^n


```julia
Xi0= rand(Float64,r,n)
```




    4×3 Array{Float64,2}:
     0.735867  0.652262  0.0855503
     0.760653  0.596169  0.512453 
     0.723474  0.941225  0.625979 
     0.51081   0.949521  0.367494 



Moment function of the sum of the Dirac measures of the points Xi0 with weights w0 and its generating series up to degree 3.


```julia
mt = moment(w0,Xi0)
s = series(mt, monoms(x, 3))
```




    1.7407335989356541 + 1.1883662719439885dx1 + 1.2876600267426495dx2 + 0.7318621666986644dx3 + 0.8317535790145825dx1^2 + 0.8525291356004889dx1*dx2 + 0.506878278461556dx1*dx3 + 1.0001509376083877dx2^2 + 0.5401157399965938dx2*dx3 + 0.35229090163175447dx3^2 + 0.5937258731159384dx1^3 + 0.5813709290890522dx1^2dx2 + 0.3591012174370174dx1^2dx3 + 0.6415254619509382dx1*dx2^2 + 0.36373653552467006dx1*dx2*dx3 + 0.24873702555048505dx1*dx3^2 + 0.8137404969282723dx2^3 + 0.4198752898125265dx2^2dx3 + 0.2578543206358955dx2*dx3^2 + 0.17810871066401893dx3^3



Decomposition of the series from its terms up to degree 3.


```julia
w, Xi = svd_decompose(s);
```


```julia
w
```




    4-element Array{Float64,1}:
     0.179634
     0.25623 
     0.491103
     0.813766




```julia
Xi
```




    4×3 Array{Complex{Float64},2}:
     0.723474+0.0im  0.941225+0.0im   0.625979+0.0im
     0.735867+0.0im  0.652262+0.0im  0.0855503+0.0im
      0.51081+0.0im  0.949521+0.0im   0.367494+0.0im
     0.760653+0.0im  0.596169+0.0im   0.512453+0.0im


