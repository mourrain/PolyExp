# PolyExp

Package for the decomposition of polynomial-exponential series.

## Introduction

Sequences ``(\sigma_{\alpha})_{\alpha} \in \mathbb{K}^{\mathbb{N}^{n}}`` or series 
```math
\sigma(y) = \sum_{\alpha \in \mathbb{K}^{\mathbb{N}^{n}}} \sigma_{\alpha} \frac{y^{\alpha}}{\alpha!}
```
which can be decomposed as polynomial-exponential series 
```math
\sum_{i=1}^r \omega_i(y) e^{\xi_{i,1} y_1+ \cdots + \xi_{i,n} y_n}
```
with polynomials ``\omega_{i}(y)`` and points ``\xi_{i}= (\xi_{i,1}, \ldots, \xi_{i,n})\in \mathbb{K}^{n}`` appear in many problems (see [Examples](@ref sec_examples)). The package `PolyExp` provides functions to manipulate (truncated) series and to compute such a decomposition from the first terms of the sequence.

## [Examples](@id sec_examples)

```@contents
Pages = map(file -> joinpath("expl", file), filter(x ->endswith(x, "md"), readdir("expl")))
```


## Functions and types

```@contents
Pages = map(file -> joinpath("code", file), filter(x ->endswith(x, "md"), readdir("code"))) 
```

## Installation

The package is available at [https://gitlab.inria.fr/mourrain/PolyExp](https://gitlab.inria.fr/mourrain/PolyExp).


To install it from Julia:
```julia
Pkg.clone("https://gitlab.inria.fr/mourrain/PolyExp.git")
```
It can then be used as follows:
```julia
using PolyExp
```
See the [Examples](@ref sec_examples) for more details.


