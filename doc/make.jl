using Documenter, PolyExp, PolyExpSDP

dir = "src"
Expl = map(file -> joinpath("expl", file), filter(x ->endswith(x, "md"), readdir(dir*"/expl")))
Code = map(file -> joinpath("code", file), filter(x ->endswith(x, "md"), readdir(dir*"/code")))

makedocs(
         format = :html,
         sitename = "PolyExp",
         authors = "B. Mourrain",
         modules = [PolyExp,PolyExpSDP],
         build = "build",
         source = dir,
         pages = Any[
                     "Home" => "index.md",
                     "Example" => Expl,
                     "Functions & types" => Code
                     ],
         repo = "https://gitlab.inria.fr/mourrain/PolyExp/tree/master",
         doctest = false
         )

deploydocs(
           deps = Deps.pip("mkdocs", "python-markdown-math"),
           repo = "gitlab.inria.fr/mourrain/PolyExp.git",
           target = "site",
           julia  = "0.5",
           osname = "osx",
           deps = nothing,
           make = nothing
           )
