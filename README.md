The package PolyExp is a julia package for the decomposition of polynomial-exponential series.


To install it within julia:

```julia
Pkg.clone("https://gitlab.inria.fr/mourrain/PolyExp.git")
```


To use it within julia:

```julia
using PolyExp
x = @ring x1 x2 x3

n = length(x)
r = 4
w = rand(Float64,r)
Xi= rand(Float64,r,n)

# Moment function of the weighted sum of Dirac measures for the points Xi with weights w
mt = moment(w,Xi)

# Generating series up to degree 3
s = series(mt, monoms(x, 3))

# Decomposition
w, Xi = svd_decompose(s)

```

## Documentation

- The package [PolyExp](http://www-sop.inria.fr/members/Bernard.Mourrain/software/PolyExp/index.html)
- For more information on [Julia](http://docs.julialang.org/en/latest/#manual)
 